import m from '../src/models';

if (!global.Reflect)
global.Reflect = {};
import './reflect';

var result = {};

result.ModelBase = m.EntityBase;

result.defaultValue = function(defaultValue){
    return function(target, name){
        var fields = target.fields || (target.fields = {});
        if (!fields[name])
            fields[name]={};
        fields[name].defaultValue = defaultValue;
    }
}

result.list = function(itemType){
    if (!itemType)
        throw "Item type should not be null";

    return function(target, name){
        var fields = target.fields || (target.fields = {});
        fields[name]={itemType:itemType};
    }
}
result.required = function(msg){
    return function(target, name){
        var fields = target.fields || (target.fields = {});
        if (!fields[name])
            fields[name]={};
        fields[name].patch = f=>f.required(msg);
    }
}

result.validate = function(validatorFunc){
    return function(target, name){
        var fields = target.fields || (target.fields = {});
        if (!fields[name])
            fields[name]={};
        fields[name].patch = f=>f.validate(validatorFunc);
    }
}

result.getter = function(target, name){
    var fields = target.fields || (target.fields = {});
    if (!fields[name])
        fields[name]={};
    fields[name].getter = true;
}

result.field = function(target, name){
    var fields = target.fields || (target.fields = {});
    if (!fields[name])
        fields[name]={};
}

result.model = function(newClass){

    var config = {};
    if (newClass.prototype.createdClass )
        config.constructor = newClass;

    var fields = newClass.prototype.fields;
    for (var fieldName in fields) {
        if (!fields.hasOwnProperty(fieldName))
            continue;

        var type = Reflect.getMetadata("design:returntype", newClass.prototype, fieldName);
        if (!type) {
            type = {};
        }
        if (type == String)
            config[fieldName] = m.str();
        else if (type.prototype && type.prototype.__isEntity)
            config[fieldName] = m.obj(type);
        else if (fields[fieldName].itemType)
            config[fieldName] = m.list(fields[fieldName].itemType);
        else
            config[fieldName] = m.field();

        if (fields[fieldName].defaultValue)
            config[fieldName].defaultValue(fields[fieldName].defaultValue)

        if (fields[fieldName].getter)
            config[fieldName].getter(newClass.prototype[fieldName]);

        if (fields[fieldName].patch)
            fields[fieldName].patch(config[fieldName]);
    }
    for (var itemName in newClass.prototype) {
        if (!newClass.prototype.hasOwnProperty(itemName))
            continue;
        if (!fields[itemName])
            config[itemName] = newClass.prototype[itemName];
    }
    var result = m.define(config, newClass.prototype.createdClass ?  newClass.prototype.createdClass : m.EntityBase);

    return result;
};

export default result;
