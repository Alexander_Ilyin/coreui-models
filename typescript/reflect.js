/**
 * Created by ilyin on 12.01.2016.
 */

(function(){
	if (typeof Reflect !== "undefined")
		return;

	var Reflect = {};
	if (typeof (window) !== "undefined")
		window.Reflect = Reflect;
	else
		global.Reflect = Reflect;

	Reflect.getMetadata = function(metaKey, target, fieldName){
		return target.__meta &&
			target.__meta[fieldName] &&
			target.__meta[fieldName][metaKey];
	}

	Reflect.metadata = function(metaKey,metaValue){
		return function (target, fieldName){
			target.__meta = target.__meta || {};
			target.__meta[fieldName] = target.__meta[fieldName] || {};
			target.__meta[fieldName][metaKey] = metaValue;
		}
	}
})();


//
//var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
//if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
//else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
//
//
//var __metadata = (this && this.__metadata) || function (k, v) {
//		if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
//	};
//
//
//__decorate([
//	def_1.field,
//	__metadata('design:type', Function),
//	__metadata('design:paramtypes', [String]),
//	__metadata('design:returntype', String)
//], PersonModel.prototype, "name", null);
//
//
//PersonModel = __decorate([
//	def_1.model,
//	__metadata('design:paramtypes', [Object])
//], PersonModel);
//
//
//var type = Reflect.getMetadata("design:returntype", newClass.prototype, fieldName);
//
//
//	Reflect.decorate(decorators, target, key, desc);
