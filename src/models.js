﻿/**
 * Created by ilyin on 25.06.2015.
 */
var Observable = require("./observable");
var Base = require("./base");
var _ = require("underscore");
var logNotify = false;

var GlobalChanges = {};
var key = 1;
var genKey = function(){ return key++;};


var depth = 0;
var beginTransaction = function(){
    depth++;
};

var globalEvents = new Observable();

var endTransaction = function(){
    depth--;
    if (depth!=0)
        return;

    var savedGlobalChanges = GlobalChanges;
    GlobalChanges = {};
    for (var p in  savedGlobalChanges) {
        beginTransaction();
        var obj = savedGlobalChanges[p];
        obj.__triggerChange();
        endTransaction();
    }

    for (var p in  savedGlobalChanges) {
        beginTransaction();
        var obj = savedGlobalChanges[p];
        obj.__triggerChanged();
        endTransaction();
    }
    globalEvents.trigger("endTransaction");
};

var isEmpty = function(json){
    for (var x in json) {
        if (json.hasOwnProperty(x))
            return false;
    }
    return true;
}



var trackStack = [];
var currentTracked = null;

var markTrackedDirty = function(obj, propMeta){
    if (!obj.__toNotify)
        return;
    var __toNotify_aboutProp = obj.__toNotify[propMeta.propName];
    if (!__toNotify_aboutProp)
        return;

    if (obj.isDirty[propMeta.propName])
        return;

    for (var targetKey in __toNotify_aboutProp) {
        if (!__toNotify_aboutProp.hasOwnProperty(targetKey))
            continue;

        var __toNotify_aboutProp_target = __toNotify_aboutProp[targetKey];
        for (var prop in __toNotify_aboutProp_target) {
            if (!__toNotify_aboutProp_target.hasOwnProperty(prop))
                continue;
            var tracked = __toNotify_aboutProp_target[prop];
            tracked[0].__markDirty(tracked[1]);
        }
    }
}

var beginTrackFor = function(obj, propMeta){
    if (!propMeta._getter)
        return;
    if (currentTracked)
        trackStack.push(currentTracked);
    currentTracked = [obj, propMeta];
}

var endTrackFor = function(obj, propMeta){
    currentTracked = trackStack.pop();
}

var trackGet = function(obj, propMeta) {
    if (!currentTracked)
        return;
    if (!obj.__toNotify)
        obj.__toNotify = {};

    var __toNotify = obj.__toNotify[propMeta.propName];
    if (!__toNotify)
        __toNotify = obj.__toNotify[propMeta.propName] = {};


    var targetKey = "K" + currentTracked[0].key;
    var __toNotify_target = __toNotify[targetKey];
    if (!__toNotify_target)
        __toNotify_target = __toNotify[targetKey] = {};

    __toNotify_target[currentTracked[1].propName] = currentTracked;
    if (logNotify)
        console.log("ToNotify updated in object ", toStr(obj, propMeta), 'set to',  notifyToStr(obj.__toNotify));

}
function notifyToStr(toNotify){
    var res = "";
    res+="\n   (\n";
    for (var prop in toNotify) {
        res += "    " + prop + "=>";
        var o = toNotify[prop];
        for (var key in o)
        {
            res += "OBJ" + key + " ";
            var propList = o[key];
            for (var propName in propList) {
                res += propName;
                res +="\n";
            }
        }
    }
    res+="   )";

    return res;
}

function toStr(obj, prop){
    var res = "";
    if (obj.data.marker)
        res += obj.data.marker;

    res += "_" + obj.key +"_";
    if (!prop)
        return "" + res + "";
    return "" + res + ":" + prop.propName + "";
}

var ModelBase = Observable.extend({
    __isModel : true,
    ___init : function(){
        this.children = {};
        this.firstCall = true;
        this.key= genKey();
        this.isDirty = {};
        this.enabledGetters = {};
        this.isChange = {};
        this.isChanged = {};
        this.errors = {};
        this.__t = 0;
        this.__version = 0;
    },

    parent : function(){
        return this.__parent;
    },

    isNested : function(name){
        var propsMeta = this.userPropsMeta[name];
        return propsMeta.itemType!=null ||   propsMeta.entityType!=null;
    },

    getVersion : function(){
        return this.__version;
    },

    onChange : function(prop, handler, context){
        this.__trackGetters();
        if (arguments.length>=2)
            this.on("change_" + prop, handler, context);
        else
            this.on("change", arguments[0], context);
    },

    unChange : function(prop, handler, context){
        if (arguments.length>=2)
            this.un("change_" + prop, handler, context);
        else
            this.un("change", arguments[0], context);
    },

    onChanged : function(prop, handler, context) {
        this.__trackGetters();
        if (typeof(prop)=="string")
            this.on("changed_" + prop, handler, context);
        else
            this.on("changed", arguments[0], arguments[1]);
    },

    unChanged : function(prop, handler, context){
        if (typeof(prop)=="string")
            this.un("changed_" + prop, handler, context);
        else
            this.un("changed", arguments[0], arguments[1]);
    },

    begin : function(){
        beginTransaction();
        this.__begin();
    },

    end : function() {
        this.__end();
        endTransaction();
    },

    __begin : function() {
        this.__t++;
    },

    __end : function(){
        this.__t--;
        if (this.__t!=0)
            return;

        if (!this.____changed)
            return;

        this.____changed = false;
        if (this.__parent) {
            this.__parent.begin();
            if (this.__parent.__markItemChanged){
                this.__parent.__markItemChanged(this.__position);
            }
            else {
                this.__parent.__markChanged(this.__parentField);
            }
            this.__parent.end();
        }
        else{
            GlobalChanges[this.key] = this;
        }
    }
})

var trackingGetters = false;
var EntityBase = ModelBase.extend({
    constructor : function(cfg, trackGetters){
        this.data = cfg || {};
        this.___init();

        if (!trackingGetters && trackGetters!==false)
        {
            trackingGetters = true;
            beginTransaction();
            this.__trackGetters();
            endTransaction();
            trackingGetters = false;
        }
    },

    __trackGetters : function(){
        if (this.__gettersTracked)
            return;

        this.__gettersTracked = true;
        for (var pName in this.userPropsMeta) {
            var propMeta = this.userPropsMeta[pName];
            if (propMeta._getter) {
                this.__getSimpleVal(propMeta, true);
            }
            else {
                var name = propMeta.propName;
                var val = this[name]();
                if (val && val.__trackGetters)
                    val.__trackGetters();
            }
        }
    },

    toJson : function(){
        var result = {};
        for (var pName in this.userPropsMeta) {
            var propMeta = this.userPropsMeta[pName];
            var name = propMeta.propName;
            var val = this[name]();
            if (val && val.toJson)
                val = val.toJson();
            if (val!==undefined && val!==null)
                result[propMeta.jsonName] = val;
        }
        return result;
    },

    getError : function(prop, deep) {
        var errors = this.getErrors(prop, deep);
        if (!errors)
            return null;

        var res = errors[prop];
        if (res) {

            var findError = function (obj) {
                for (var key in obj) {
                    if (key != "childErrors")
                        return obj[key];
                }
                if (obj.childErrors)
                    for (var key in obj.childErrors) {
                        var childErrors = obj.childErrors[key];
                        if (childErrors)
                            return findError(childErrors);
                    }
            }
            return findError(res);
        }
        return null;
    },

    hasErrors : function(prop, deep){
        return !isEmpty(this.getErrors(prop, deep));
    },

    getErrors : function(prop, deep){
        var result = null;
        if (deep===undefined)
            deep = true;
        var props = prop ? [prop] : this.__userPropNames;
        for (var i = 0; i < props.length; i++) {

            var propName = props[i];
            var propMeta = this.propsMeta[propName];
            var errorPropMeta = this.propsMeta[propName + "__error"];
            var errors = this[errorPropMeta.propName].call(this);

            if (deep) {
                if (propMeta.entityType) {
                    var nested = this.__getObjVal(propMeta);
                    var nestedErrors = nested.getErrors(null, true);
                    if (!isEmpty(nestedErrors)) {
                        errors = errors || {};
                        errors.childErrors = nestedErrors;
                    }
                }
                if (propMeta.itemType) {
                    var array = this.__getListVal(propMeta);
                    var nestedErrors = array.getErrors(null, true);
                    if (!isEmpty(nestedErrors)) {
                        errors = errors || {};
                        errors.childErrors = nestedErrors;
                    }
                }
            }
            if (errors) {
                if (!result)
                    result = {};
                result[propName] = errors;
            }
        }
        return result;
    },

    clearError : function(prop, code) {
        this.setError(prop, {code : code});
    },

    setError : function(prop, error){
        if (typeof(arguments[1])=='string') {
            error = {msg:error, code : 'default'};
        }
        if (error && !error.code)
            error.code = 'default';

        var customErrorFunc = this[prop +"_customErrors"];
        var currentError = customErrorFunc.call(this) || {};
        if (!error.msg)
            delete currentError[error.code];
        else
            currentError[error.code] = error;

        var value = isEmpty(currentError) ? null : currentError;
        customErrorFunc.call(this, value);
    },

    __setSimpleVal : function(propMeta, val){
        this.begin();
        var prop = propMeta.jsonName;
        if (propMeta._setter && val){
            val = val || null;
            val = propMeta._setter(val);
            if (val===undefined) {
                this.end();
                return;
            }
        }

        if (this.data[prop]!==val) {
            this.enabledGetters[prop] = false;
            this.__markDirty(propMeta, true);
            this.data[prop] = val;
            this.data[prop + "_customErrors"] = null;
            this.__markChanged(propMeta);
        }
        this.end();
    },

    __getSimpleVal : function(propMeta, trackGetters) {
        trackGet(this, propMeta);
        var prop = propMeta.jsonName;
        if (propMeta._getter && this.__isGetterEnabled(propMeta)) {
            var initialVal = this.data[prop];
            if (trackGetters || this.isDirty[prop] || initialVal===undefined ){
                delete this.isDirty[prop];
                beginTrackFor(this, propMeta);
                var newVal = propMeta._getter.call(this);
                if (!trackGetters || initialVal==null)
                    this.data[prop] = newVal;
                endTrackFor(this, propMeta);
            }
            return this.data[prop];
        }
        else if (propMeta._defaultValue!==undefined) {
            if (this.data[prop]===undefined)
                return propMeta._defaultValue;
        }
        return this.data[prop];
    },

    hasGetter : function(propName) {
        var propMeta = this.propsMeta[propName];
        if (!propMeta)
            throw "Field not found " + propName;
        return !!propMeta._getter;
    },

    getterEnabled : function(){
        var name = arguments[0];
        var propMeta = this.propsMeta[name];

        if (arguments.length>=2)  {
            var val = arguments[1];
            var currentValue = this.__isGetterEnabled(propMeta);
            if (currentValue==val) {
                return;
            }

            this.begin();
            this.enabledGetters[name] = !!(val);
            this.__markDirty(propMeta);
            this.__markChanged(propMeta);
            this.end();
        }
        else
        {
            return this.__isGetterEnabled(propMeta);
        }
    },

    __isGetterEnabled: function(propMeta){
        if (!propMeta.getter)
            return false;
        var instanceVal = this.enabledGetters[propMeta.propName];
        if (instanceVal===undefined)
            return propMeta.getterIsEnabledByDefault;
        return !!instanceVal;
    },



    __setListVal : function(propMeta, newArr) {
        var list = this.__getListVal(propMeta);
        list.__update(newArr);
    },

    __getListVal : function(propMeta) {
        trackGet(this, propMeta);
        var propName = propMeta.jsonName;
        var child = this.children[propName];
        if (!child){
            var childData = this.data[propName];
            if (!childData) {
                this.data[propName] = childData = [];
            }
            child = childData;
            for (var func in ListProto)
                if (ListProto.hasOwnProperty(func))
                    child[func] = ListProto[func];
            child.itemType = propMeta.getItemType();
            child.___init();
            child.__parent = this;
            child.__parentField = propMeta;
            child.updateItems();
            this.children[propName] = child;
        }
        return child;
    },

    __getObjVal : function(propMeta){
        var propName = propMeta.jsonName;
        var child = this.children[propName];
        if (!child){
            var childData = this.data[propName];
            if (!childData)
                this.data[propName] = childData = {};

            var entityType = propMeta.getEntityType();
            child = new entityType(childData, false);
            child.__parent = this;
            child.__parentField = propMeta;
            this.children[propName] = child;
            child.__trackGetters();
        }
        return child;
    },

    __setObjVal : function(propMeta, newVals){
        this.begin();
        var child = this.__getObjVal(propMeta);
        child.__update(newVals);
        this.end();
    },

    update : function(newVals) {
        this.begin();
        this.__update(newVals);
        this.end();
    },

    setFields : function(newVals){
        this.begin();
        for (var field in newVals) {
            var func = this.funcFor(field);
            if (func)
                func.call(this, newVals[field]);
        }
        this.end();
    },

    __update : function(newVals){
        newVals = newVals || {};
        for (var field in newVals) {
            var func = this.funcFor(field);
            if (func)
                func.call(this, newVals[field]);
        }
        for (var d in this.data) {
            if (!newVals.hasOwnProperty(d))
            {
                var meta = this.metaFor(d);
                if (meta){
                    if (!this.__isGetterEnabled(meta))
                        this.funcFor(d).call(this, null);
                }
            }
        }
    },

    __markDirty : function(propMeta, onlyDeps) {
        markTrackedDirty(this, propMeta);
        var propName = propMeta.propName;
        if (!onlyDeps && propMeta._getter)
            this.isDirty[propName] = true;

        delete this.errors[propName];
        if (this.__parent){
            if (this.__parent.__markDirty)
                this.__parent.__markDirty(this.__parentField, onlyDeps);
            else  if (this.__parent.__itemDirty)
                this.__parent.__itemDirty(this, onlyDeps);
        }
        this.__markChanged(propMeta);
    },

    __markChanged : function(propMeta){
        this.isChanged[propMeta.propName] = true;
        this.isChange[propMeta.propName] = true;
        this.____changed = true;
        this.__version++;
    },

    __triggerChange : function() {
        var changed = this.isChange;
        var newVal;
        this.isChange = {};
        for (var prop in  changed) {
            var event = "change_" + prop;
            if (this.children[prop]){
                var newVal = this[prop].call(this);
                this.trigger(event, newVal);
                if (newVal && newVal.__triggerChange)
                    newVal.__triggerChange.call(newVal);
            }
            else {
                if (this.hasListener(event)) {
                    var newVal = this[prop].call(this);
                    this.trigger(event, newVal);
                }
            }
            var propMeta = this.propsMeta[prop];
            if (propMeta.onChangeHandlers.length) {
                var v = newVal || this[prop].call(this);
                propMeta.tiggerOnChange(this, v);
            }
        }
        this.trigger("change");
    },

    __triggerChanged : function() {
        var changed = this.isChanged;
        this.isChanged = {};
        for (var prop in  changed) {
            var event = "changed_" + prop;

            if (this.children[prop]){
                var newVal = this[prop].call(this);
                this.trigger(event, newVal);
                if (newVal && newVal.__triggerChanged)
                    newVal.__triggerChanged.call(newVal);
            }
            else {
                if (this.hasListener(event)) {
                    var newVal = this[prop].call(this);
                    this.trigger(event, newVal);
                }
            }
        }
        this.trigger("changed");
    },

    __getPosition : function(){
        return this.__position;
    }
});

var ListProto = _.extend({}, ModelBase.prototype, {
    wrap : function(item) {
        var wrap = item.__isModel ? item : new this.itemType(item, false);
        if (!wrap.__parent)
            wrap.__parent = this;
        else if (wrap.__parent!=this)
            throw "Item is laready in use";
        wrap.__trackGetters();
        return wrap;
    },

    __trackGetters : function(){
        for (var i = 0; i < this.length; i++) {
            this[i].__trackGetters();
        }
    },

    toJson : function(){
        return _.map(this, function(x){return x.toJson()});
    },

    __itemDirty : function(item, onlyDeps){
        this.isDirty[item.__position] = true;
        this.isChanged[item.__position] = true;
        this.isChange[item.__position] = true;
        if (this.__parent)
            if (this.__parent.__markDirty)
                this.__parent.__markDirty(this.__parentField, onlyDeps);
    },

    get : function(i)
    {
        return this[i];
    },

    select : function(func){
        return _.map(this, func);
    },

    remove : function(item){
        var pos = _.indexBy(this, item);
        this.splice(pos, 1);
    },
    last : function(){
        return this.length ? this[this.length-1] : null;
    },

    map : function(func, context) {        return  _.map(this, func, context);    },

    push : function(item) {
        var r =  this.__updateFunc(function(){
            item = this.wrap(item);
            var newLength = Array.prototype.push.call(this, item);
            item.__position = newLength-1;
            return newLength;
        }, arguments, true);
        return r;
    },

    add : function(item) {
        var r =  this.__updateFunc(function(){
            item = this.wrap(item);
            var newLength = Array.prototype.push.call(this, item);
            item.__position = newLength-1;
            return item;
        }, arguments, true);
        return r;
    },
    __markItemChanged : function(index) {
        this.____changed = true;
        this.isChanged[index] = true;
        this.isChange[index] = true;
    },

    pop : function(){
        var result = Array.prototype.pop.apply(this, arguments);
        return result;
    },

    reverse : function(){
        return this.__updateFunc(function() {
            return  Array.prototype.reverse.apply(this, arguments);
        }, arguments, true);
    },

    shift : function(){
        return this.__updateFunc(function() {
            return  Array.prototype.shift.apply(this, arguments);
        }, arguments, true);

    },
    slice : function(){
        var result = Array.prototype.slice.apply(this, args);
        return result;
    },

    sort : function(){
        return this.__updateFunc(function() {
            return  Array.prototype.sort.apply(this, arguments);
        }, arguments, true);
    },

    splice : function(){
        return this.__updateFunc(function() {
            return  Array.prototype.splice.apply(this, arguments);
        }, arguments, true);
    },

    unshift : function(){
        return this.__updateFunc(function() {
            return Array.prototype.unshift.apply(this, arguments);
        }, arguments, true);
    },

    updateItems : function(){
        for (var i=0;i<this.length;i++) {
            this[i] = this.wrap(this[i]);
            this[i].__position = i;
        }
    },
    update : function(items){
       return this.__update(items);
    },

    __updateFunc : function(func, args, update) {
        this.begin();
        var result = func.apply(this, args);
        if (update)
            this.updateItems();

        if (this.__parent) {
            this.__parent.__markDirty(this.__parentField, true);
        }

        this.trigger("change", this);
        this.____changed = true;
        this.end();
        return result;
    },

    __update : function(items){
        this.__updateFunc(function(){
            var args = items.concat([]);
            args.unshift(0, this.length);
            Array.prototype.splice.apply(this, args);
        }, arguments, true);
    },

    __triggerChange : function(){
        var changed = this.isChange;
        this.isChange = {};
        for (var prop in  changed) {
            var newVal = this[prop];
            if (newVal && newVal.__triggerChange)
                newVal.__triggerChange.call(newVal);
        }
        this.trigger("change");
    },

    __triggerChanged : function(){
        var changed = this.isChanged;
        this.isChanged = {};
        for (var prop in  changed) {
            var newVal = this[prop];
            if (newVal && newVal.__triggerChanged)
                newVal.__triggerChanged.call(newVal);
        }
        this.trigger("changed");
    },

    getErrors : function(){
        var result = {};
        for (var i = 0; i < this.length; i++) {
            var item = this[i];
            var nestedErrors = item.getErrors(null, true);
            if (!isEmpty(nestedErrors))
                result[i] = nestedErrors;
        }
        return result;
    },

    setError : function(prop, msg, code){
        code = code || 'default';
        this.errors[prop] = this.errors[prop] || {};
        this.errors[prop][code] = {msg:msg, code:code};
    },
});

var messages = {
    FieldIsRequired :    function ()          { return "Поле является обязательным для заполнения"; },
    FieldTooLong :       function (maxLength) { return "Значение должно содержать не более " + maxLength + " символов"; },
    FieldLengthNotEqual: function (length)    { return "Значение должно содержать ровно " + length + " символов"; },
    FieldTooShort:       function (minLength) { return "Значение должно содержать не менее " + minLength + " символов"; },
    FieldInvalidFormat:  function ()          { return "Значение не соответствует формату"; },
    FieldInvalidEmailFormat: function()       { return "Не соответствует формату электронной почты"; },
    FieldInvalidPhoneFormat: function()       { return "Номер телефона должен состоять из 10 цифр"; },
    FieldInvalidGuidFormat:  function()       { return "Не соответствует формату GUID"; },
}

var CommonFormatRegexes = {
    email: /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i, // not RFC compliant, IDN compatible. It is confirmed by sending a mail to it, so we won't bother much right here
    phone: /^( *\d *){10}$/,
    guid: /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i
}

var FieldDefinition = Base.extend({
    isField : true,

    constructor : function(cfg){
        this.base(cfg);
        this.onChangeHandlers = [];
        this._dep = [];
        this._mustUpdate = [];
        this._validators = [];
    },

    required : function(errorMessage){
        this._validators.push(function(v){
            if (v===null || v==="" || v===undefined)
                return {
                        msg : errorMessage || messages.FieldIsRequired(),
                        code : "Required"
                    };
        });
        return this;
    },

    maxLength: function (maxFieldLength, errorMessage) {
        this._validators.push(function(v){
            if (v && v.length > maxFieldLength)
                return {
                    msg : errorMessage || messages.FieldTooLong(maxFieldLength),
                    code : "TooLong"
                };
        });
        return this;
    },

    notWhitespace: function(errorMessage) {
        this._validators.push(function(v){
            if ((v !== 0) && (!v))
                return {
                    msg:  errorMessage || messages.FieldIsRequired(),
                    code: "Whitespace"
                };
            if (!/[^\s]/.test(v))
                return {
                    msg:  errorMessage || messages.FieldIsRequired(),
                    code: "Whitespace"
                };
        });
        return this;
    },

    lengthEqual: function (length, errorMessage) {
        this._validators.push(function(v){
            if (v && v.length != length)
                return {
                    msg:  errorMessage || messages.FieldLengthNotEqual(length),
                    code: "Length"
                };
        });
        return this;
    },

    minLength: function (minLength, errorMessage) {
        this._validators.push(function(v){
            if (v && v.length < minLength)
                return {
                    msg:  errorMessage || messages.FieldTooShort(minLength),
                    code: "Length"
                };
        });
        return this;
    },

    regex: function (regex, errorMessage) {
        this._validators.push(function(v){
            if (v && !regex.test(v))
                return {
                    msg:  errorMessage || messages.FieldInvalidFormat(),
                    code: "Regex"
                };
        });
        return this;
    },

    email: function (errorMessage) {
        return this.regex(CommonFormatRegexes.email, errorMessage || messages.FieldInvalidEmailFormat());
    },

    phone: function (errorMessage) {
        return this.regex(CommonFormatRegexes.phone, errorMessage || messages.FieldInvalidPhoneFormat());
    },

    guid: function (errorMessage) {
        return this.regex(CommonFormatRegexes.guid, errorMessage || messages.FieldInvalidGuidFormat());
    },

    validate : function(func){
        this._validators.push(func);
        return this;
    },

    onChange : function(handler){
        this.onChangeHandlers.push(handler);
        return this;
    },

    setter : function(func){
        this._setter = func;
        return this;
    },

    getter : function(func, enabledByDefault){
        this.getterIsEnabledByDefault = enabledByDefault===undefined ? true : !!(enabledByDefault);
        this._getter = func;
        return this;
    },

    jsonField : function(x) {
        this._jsonField = x;
        return this;
    },

    defaultValue : function(x){
        this._defaultValue = x;
        return this;
    }
});

var EntityMeta = Base.extend({
    constructor : function(cfg){
        this.base(cfg);
    }
});

var PropMeta = Base.extend({
    constructor : function(def){
        this.base(def);
        this.jsonName = def._jsonField || def.propName;
    },

    getItemType : function(){
        if (this.itemType.prototype.__isEntity)
            return this.itemType;
        this.itemType = this.itemType();
        return this.itemType;
    },

    getEntityType : function(){
        if (this.entityType.prototype.__isEntity)
            return this.entityType;
        this.entityType = this.entityType();
        return this.entityType;
    },

    tiggerOnChange : function(context, newVal){
        if (this.onChangeHandlers)
            _.forEach(this.onChangeHandlers, function(h){
                h.call(context, newVal);
            })
    },

    getEnumValues : function(){
        return this._enumValues;
    }
});
EntityBase.prototype.createdClass = EntityBase;
EntityBase.prototype.__isEntity = true;

var jsModels = {
    globalEvents: globalEvents,
    EntityBase : EntityBase,
    messages: messages,
    begin : beginTransaction,
    end : endTransaction,
    field : function(){        var def = new FieldDefinition();        return def;   },
    str : function(){ return this.field()},
    date : function(){ return this.field()},
    bool : function(){ return this.field()},
    num : function(){ return this.field()},
    en : function(values){

        if (!values) {
            warn("Invalid enum declaration");
            values = [];
        }
        var validValues = {};
        var enumValues = _.map(values, function(x) {

            if (!x)
                throw 'Invalid enum declaration';
            if (typeof(x) == 'string')
                return {key: x, value: x}
            if (x.key) {
                if (!x.value)
                    x.value = x.key;
                return x;
            }
            if (x.length)
                return {key : x[0], value:x[1] || x[0]};
        });

        enumValues = _.reduce(enumValues, function(result, x){result[x.key]=x;return result;}, {});
        var field = this.field().setter(function(val) {
            if (val==null)
                return val;
            if (enumValues[val])
                return val;
            else
                console.log("Invalid val passed ", val, "enumValues are ", enumValues);
            return undefined;
        });
        field._enumValues = enumValues;
        return field;
    },

    obj : function(entityType){       return new FieldDefinition({entityType :entityType });    },
    list : function(itemType){

        return new FieldDefinition({
            itemType :itemType
        });
    },

    define : function(cfg, base){
        if (base){
            var newCfg = _.extend({}, base.prototype.sourceCfg, cfg);
            if (cfg.constructor)
                newCfg.constructor = cfg.constructor;
            cfg = newCfg;
        }
        var entityMeta = new EntityMeta(cfg);
        var propsMeta = {};
        var userPropsMeta = {};
        var newPrototype = {};
        var jsonToFuncs = {};
        var jsonToMeta = {};
        var propsWithGettersAndMonitoring = null;
        var propsWithGetters = [];

        var fullCfg = {};
        fullCfg = _.extend(fullCfg, cfg);

        _.forEach(cfg, function(x,propName) {
            var fieldDef = cfg[propName];
            if (!fieldDef || !(fieldDef instanceof (FieldDefinition))) {
                newPrototype[propName] = fieldDef;
                return;
            }

            var customErrorField = new FieldDefinition();
            customErrorField.isErrorField = true;
            var customErrorPropName = (propName + "_customErrors");
            fullCfg[customErrorPropName] = customErrorField;


            var errorField = new FieldDefinition();
            errorField.getter(function(){
                var result = null;
                var value = this[propName].call(this);
                for (var i = 0; i < fieldDef._validators.length; i++) {
                    var error = fieldDef._validators[i].call(this, value);
                    if (!error)
                        continue;

                    if (!result)
                        result = {};

                    if (typeof(error)=='string')
                        error = {
                            msg : error,
                            code :"default"
                        };

                    if (error) {
                        result[error.code]=error;
                    }
                }

                var customErrors = this[customErrorPropName].call(this);
                if (customErrors)
                {
                    if (!result)
                        result = {};
                    _.extend(result, customErrors);
                }
                return result;
            });
            errorField.isErrorField = true;
            fullCfg[propName + "__error"] = errorField;

        });

        function hasGetterInside(propMeta){
            if (propMeta._getter)
                return true;
            if (propMeta.itemType){
                if (propMeta.getItemType().prototype.propsWithGetters.length>0)
                    return true;
            }
            if (propMeta.entityType){
                if (propMeta.getEntityType().prototype.propsWithGetters.length>0)
                    return true;
            }
            return false;
        }

        _.forEach(fullCfg, function(x,propName){
            var fieldDef = fullCfg[propName];
            if (!fieldDef || !(fieldDef instanceof (FieldDefinition))) {
                newPrototype[propName] = fieldDef;
                return;
            }

            fieldDef.propName = propName;
            var propMeta = new PropMeta(fieldDef);
            propsMeta[propName] = propMeta;
            if (!fieldDef.isErrorField)
                userPropsMeta[propName] = propMeta;

            if (fieldDef.itemType) {
                newPrototype[propName] = function () {
                    if (arguments.length > 0)
                        this.__setListVal(propMeta, arguments[0]);
                    else
                        return this.__getListVal(propMeta);
                }
            }
            else if (fieldDef.entityType) {
                newPrototype[propName] = function () {
                    if (arguments.length > 0)
                        this.__setObjVal(propMeta, arguments[0]);
                    else
                        return this.__getObjVal(propMeta);
                }
            }
            else {
                newPrototype[propName] = function () {
                    if (arguments.length > 0)
                        this.__setSimpleVal(propMeta, arguments[0]);
                    else
                        return this.__getSimpleVal(propMeta);
                }
            }
            newPrototype[propName].meta = propMeta;
            newPrototype[propName].propName = propName;
            jsonToFuncs[propMeta.jsonName] = newPrototype[propName];
            jsonToMeta[propMeta.jsonName] = propMeta;

            if (!fieldDef.isErrorField)
                if (propMeta._getter) {
                    propsWithGetters.push(propMeta);
                }
        });

        var getPropsWithGettersAndMonitoring = function(x,propName) {
            if (propsWithGettersAndMonitoring)
                return propsWithGettersAndMonitoring;
            propsWithGettersAndMonitoring = [];
            _.forEach(fullCfg, function(x,propName){
                var fieldDef = fullCfg[propName];
                var propMeta = propsMeta[propName];
                if (propMeta.onChangeHandlers.length && hasGetterInside(propMeta)){
                    propsWithGettersAndMonitoring.push(propMeta);
                }
            });
            return propsWithGettersAndMonitoring;
        };


        newPrototype.__propNames = _.keys(propsMeta);
        newPrototype.__userPropNames = _.keys(userPropsMeta);
        newPrototype.propsMeta = propsMeta;
        newPrototype.userPropsMeta = userPropsMeta;
        newPrototype.propsWithGetters = propsWithGetters;
        newPrototype.__isEntity = true;
        newPrototype.getPropsWithGettersAndMonitoring = getPropsWithGettersAndMonitoring;
        newPrototype.metaFor = function(x){
            return jsonToMeta[x];

        };
        newPrototype.isSimpleProp = function(propName) {
            return !userPropsMeta[propName].entityType && !userPropsMeta[propName].itemType;
        };

        newPrototype.funcFor = function(x){
            return jsonToFuncs[x];
        };
        newPrototype.sourceCfg=cfg;

        var createdClass = (base || EntityBase).extend(newPrototype);
        createdClass.prototype.createdClass = createdClass;
        return createdClass;
    }
};

module.exports = jsModels;