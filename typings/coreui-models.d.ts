declare module "coreui-models" {
    class Model {
        static globalEvents : any
    }
    export = Model
}

declare module "coreui-models/typescript" {
    export function model(c:any):any;
    export function field(target:any, key:string, value:any);
    export function defaultValue(val:any):Function;
    export function required(val?:any):Function;
    export function validate(func:Function):Function;
    export function getter(target:any, key:string, value:any);
    export function list(type:any):Function;

    export class ModelBase {
        constructor(json?:any);
        hasErrors():boolean;
        update(json?:any);
        begin();
        trigger(eventName: string, args:any);
        end();
        onChanged(propName:string, callback:Function);
        onChange(propName:string, callback:Function);
    }

    export class ModelArray<T> extends Array<T>{
        push(item:T|any);
        add(item:T|any):T;
    }
}




