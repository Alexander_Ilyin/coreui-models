
var assert = require("assert");
var m = require("../src/models");


test("t22. no circular deps", function(){
    var MyModel = m.define({
        s : m.str(),
        s1 : m.str()
    });

    var myModel = new MyModel();
    var i = 0;
    myModel.onChange("s", function(){ myModel.s1("sd" + (++i))});
    myModel.s("test1");
    myModel.s("test2");
    assert.equal(myModel.s1(), "sd2");
});

test("t21. disable disabled getters does not raise on change", function(){
    var MyModel = m.define({
        s : m.str().getter(function(){ return "true"})
    });

    var myModel = new MyModel();
    var triggered = false;
    myModel.onChange(function(){triggered = true;});
    myModel.getterEnabled('s', true);
    assert.equal(triggered, false);
    myModel.getterEnabled('s', false);
    assert.equal(triggered, true);
});

test("t20. inheritance is supported", function(){
    var Date = m.define({
        constructor : function(){
            this.base.apply(this, arguments);
            this.month("Feb");
        },
        month : m.en([
            {key:'Jan',value:'January'},
            {key:'Feb',value:'February'}
        ]),

        getMonth : function(){
            return "Month is " + this.month()
        }
    });

    var Date2 = m.define({
        constructor : function(){
            this.base.apply(this, arguments);
        },
        getMonth : function(){
            return "Super " + this.base();
        }
    }, Date);

    var d = new Date2();
    assert.equal(d.getMonth(),'Super Month is Feb');
});

test("t19. inheritance is supported", function(){
    var Date = m.define({
        constructor : function(){
            this.base.apply(this, arguments);
            this.month("Feb");
        },
        month : m.en([
            {key:'Jan',value:'January'},
            {key:'Feb',value:'February'}
        ]),

        getMonth : function(){
            return "Month is " + this.month()
        }
    });

    var Date2 = m.define({
        getMonth : function(){
            return "Super " + this.base();
        }
    }, Date);

    var d = new Date2();
    assert.equal(d.getMonth(),'Super Month is Feb');
});

test("t18. constructors and methods are supported", function(){
    var Date = m.define({
        constructor : function(){
            this.base.apply(this, arguments);
            this.month("Feb");
        },
        month : m.en([
            {key:'Jan',value:'January'},
            {key:'Feb',value:'February'}
        ]),

        getMonth : function(){
            return "Month is " + this.month()
        }
    });

    var d = new Date();
    assert.equal(d.getMonth(),'Month is Feb');
});


test("t17. json enums are supported", function(){
    var Date = m.define({
        month : m.en([
                {key:'Jan',value:'January'},
                {key:'Feb',value:'February'}
        ])
    });
    var d = new Date();
    d.month('Jan');
    assert.equal(d.month(),'Jan');
    d.month('Feb');
    assert.equal(d.month(),'Feb');
    d.month('Hello');
    assert.equal(d.month(),'Feb');
});

test("t16. array enums are supported", function(){
    var Date = m.define({
        month : m.en([
            ['Jan','January'],
            ['Feb','February']
        ])
    });
    var d = new Date();
    d.month('Jan');
    assert.equal(d.month(),'Jan');
    d.month('Feb');
    assert.equal(d.month(),'Feb');
    d.month('Hello');
    assert.equal(d.month(),'Feb');
});

test("t15. simple enums are supported", function(){
    var Date = m.define({
        month : m.en(['Jan', 'Feb'])
    });
    var d = new Date();
    d.month('Jan');
    assert.equal(d.month(),'Jan');
    d.month('Feb');
    assert.equal(d.month(),'Feb');
    d.month('Hello');
    assert.equal(d.month(),'Feb');
});

test("t14. newly added object support change tracking", function(){
    var log = "";

    var DocumentRow = m.define({
        title : m.field().required().defaultValue("Doc"),
        parentVal : m.field().getter(function(){
            return this.parent().parent().val()
        })
    });

    var Document = m.define({
        rows : m.list(DocumentRow),
        val :  m.field()
    });

    var d = new Document({val :'D1'});
    d.rows().onChanged(function(){
        log += " rows changed";
    });
    d.rows().push({title: 'R1'});
    assert.equal(log, " rows changed");
    var last = d.rows().last();
    d.val("345");
    assert.equal(log, " rows changed rows changed");
    d.val("347");
    assert.equal(log, " rows changed rows changed rows changed");
});

test("t12. cycles work just fine", function(){
    var DocumentRow = m.define({
        sum : m.field().getter(function(){
            return this.tax()/0.18;
        }),

        tax: m.field().getter(function(){
            return this.sum()*0.18;
        }, false)
    });

    var row = new DocumentRow();
    row.tax(18);
    assert.equal(row.sum(), 100);
    row.tax(36);
    assert.equal(row.sum(), 200);
});


test("t11. dead objects should not receive changes", function() {
    var log = "";

    var DocumentRow = m.define({
        title : m.field().required().defaultValue("Doc"),
        parentVal : m.field().getter(function(){
            return this.parent().parent().val()
        })
    });

    var Document = m.define({
        rows : m.list(DocumentRow),
        val :  m.field()
    });

    var d = new Document({val :'D1'});
    d.rows().push({title: 'R1'});
    var r1 = d.rows().last();
    r1.onChanged(function(){
        log += " changed to " + r1.parentVal();
    });
    d.val("V1");
    d.val("V2");
    assert.equal(log, " changed to V1 changed to V2");
    d.rows([]);
    d.val("V3");
    assert.equal(log, " changed to V1 changed to V2");
});

test('t10. child items should trigger onchange.', function() {
    var Address = m.define({         street : m.field(),         house : m.field()     });
    var Person = m.define({         name : m.field(),         lastName : m.field(),        addressList : m.list(Address)    });
    var p = new Person();
    var addressList = p.addressList();
    var a1 = new Address();
    addressList.push(a1);

    var log = "";
    p.onChange(function(){ log+= " person changed"});
    a1.street("S3");
    assert.equal(log, " person changed");
});

test("t9. initial values do not affect recalculations", function() {
    var Person = m.define({
        name : m.field(),
        lastName : m.field().getter(function(){
            return this.name() + "_LAST";
        })
    });

    var p = new Person({name:"A", lastName:"A_LAST"});
    p.name("NNN");
    assert.equal(p.lastName(), "NNN_LAST");
});

test("t8. enabled getter does not affect deps", function() {
    var Person = m.define({
        name : m.field(),
        lastName : m.field().getter(function(){
            return this.name() + "_LAST";
        })
    });


    var p = new Person();
    p.name("XXX");
    p.lastName();
    p.getterEnabled("name", false);
    p.lastName();
    p.name("NNN");
    assert.equal(p.lastName(), "NNN_LAST");
});


test("t7. functions are supported in declarations", function() {
    var log = "";

    var DocumentRow = m.define({
        item : m.obj(function(){ return DocumentRowItem;}).onChange(function(v){ log +=" item change"}),
        val : m.field()
    });

    var DocumentRowItem = m.define({
        parentVal : m.field().getter(function(){
            return this.parent().val();
        })
    });

    var r1 = new DocumentRow({item: {}});
    assert.equal(log, "");
    r1.val("345");
    assert.equal(log, " item change");
});

test("t6. onChange is supported for nested objects with calculated getters", function() {
    var log = "";
    var DocumentRowItem = m.define({
        parentVal : m.field().getter(function(){
            return this.parent().val();
        })
    });
    var DocumentRow = m.define({
        item : m.obj(DocumentRowItem).onChange(function(v){ log +=" item change"}),
        val : m.field()
    });
    var r1 = new DocumentRow({item: {}});
    assert.equal(log, "");
    r1.val("345");
    assert.equal(log, " item change");
});

test("t5. onChange is supported for nested objects", function() {
    var log = "";

    var DocumentRowItem = m.define({
        val : m.field()
    });

    var DocumentRow = m.define({
        item : m.obj(DocumentRowItem).onChange(function(v){ log +=" item change"}),
        val : m.field()
    });
    var r1 = new DocumentRow({item: {}});
    assert.equal(log, "");

    r1.item().val("345");
    assert.equal(log, " item change");
});

test("t4. onChange is supported for nested lists", function() {
    var log = "";

    var DocumentRowItem = m.define({
        val : m.field()
    });

    var DocumentRow = m.define({
        list : m.list(DocumentRowItem).onChange(function(v){ log +=" list changed"}),
        val : m.field()
    });
    var r1 = new DocumentRow({list: [{}]});
    assert.equal(log, "");

    r1.list()[0].val("345");
    assert.equal(log, " list changed");
});


test("t3. onChange for property is supported", function() {
    var log = "";

    var DocumentRow = m.define({
        title : m.field().onChange(function(v){ log +="title changed " + v}),
        val : m.field()
    });
    var r1 = new DocumentRow({title: 'R1'});
    r1.val(34);
    assert.equal(log, "");
    r1.title("Title1");
    assert.equal(log, "title changed Title1");
});

test("t2. if onChanged handler changes object then onChanged re-triggered", function() {
    var log = "";

    var DocumentRow = m.define({
        title : m.field(),
        val : m.field()
    });
    var r1 = new DocumentRow({title: 'R1'});
    r1.onChanged("title", function(){ log+=" title changed"; r1.val("v1");  });
    r1.onChanged("val", function(){ log+=" val changed"; });
    r1.title("Title1");
    assert.equal(log, " title changed val changed");
});


test("t1. unitialized objects should send change notifications", function() {
    var log = "";

    var DocumentRow = m.define({
        title : m.field().required().defaultValue("Doc"),
        parentVal : m.field().getter(function(){
            return this.parent().parent().val()
        })
    });

    var Document = m.define({
        rows : m.list(DocumentRow),
        val :  m.field()
    });

    var d = new Document({val :'D1'});
    d.rows().push({title: 'R1'});
    var r1 = d.rows().last();
    r1.onChanged(function(){
        log += " changed to " + r1.parentVal();
    });
    d.val("V1");
    assert.equal(log, " changed to V1");
});


test("magic cycle", function() {
    var Region = m.define({
        regionName: m.field().validate(function(){
            if (!this.code() && !this.regionName())
                return;
            if (this.code() && this.regionName())
                return;
            return "error";
        }),

        code: m.field().validate(function(){
            if (!this.code() && !this.regionName())
                return;
            if (this.code() && this.regionName())
                return;
            return "error";
        })
    });

    var region = new Region({code :"A", regionName : "B"});
    assert.deepEqual(region.getErrors(null, true), null);

    region.update({code:"234"});
    assert.deepEqual(region.getErrors(null, true), {
        regionName: { 'default': { msg: 'error', code: 'default' } },
        code: { 'default': { msg: 'error', code: 'default' } }
    });
    region.update({code:null});
    assert.deepEqual(region.getErrors(null, true), null);
});


test("parent dependencies should work in validation", function() {
    var Street = m.define({
        name: m.field().validate(function(v){
            return this.parent().parent().regionName()!=v ? null : "FAIL";
        })
    });

    var Region = m.define({
        regionName: m.field(),
        streets : m.list(Street)
    });

    var region = new Region();
    region.streets([
        {name : "s1"},
        {name : "s2"}
    ]);
    assert.deepEqual(region.streets().get(0).getErrors("name"), null);
    region.regionName("s1");
    assert.deepEqual(region.streets().get(0).getErrors("name"), { name: {'default':{ msg: 'FAIL', code: 'default' } } });
});

test("t27. model supports hasErrors", function() {
    var Square = m.define({
        size: m.field().defaultValue(0).required()
    });

    var s = new Square();
    s.setError("size", {msg:"ERR", code :"errCode"});
    assert.equal(s.hasErrors(), true);
    s.clearError("size", "errCode");
    assert.equal(s.hasErrors(), false);
});

test("t39. setError supports json", function() {
    var Square = m.define({
        size: m.field().defaultValue(0).required()
    });

    var s = new Square();
    s.setError("size", {msg:"ERR", code :"errCode"});
    assert.deepEqual(s.getError("size"), { msg: 'ERR', code: 'errCode' });
});

test("t49. validation on auto fields returns errors", function() {
    var Square = m.define({
        size: m.field().defaultValue(0).required(),
        area: m.field().getter(function () {
            return this.size() * this.size();
        }).validate(function (v) {
            if (v > 400)
                return {
                    msg: "Area is too big"
                };
        })
    });

    var s = new Square();
    s.size(10);
    assert.equal(s.getError("area"), null);
    s.size(30);
    assert.notEqual(s.getError("area"), null);
});

test("by default getterEnabled should return true", function(){
    var DocumentRow = m.define({
        sum : m.field(),
        tax: m.field().getter(function(){             return this.sum()*0.18;         })
    });
    var row = new DocumentRow();
    row.sum(100);
    assert.equal(row.getterEnabled("tax"), true);
});

test("prop should return auto value when getter is disabled", function(){
    var DocumentRow = m.define({
        sum : m.field(),
        tax: m.field().getter(function(){             return this.sum()*0.18;         })
    });
    var row = new DocumentRow();
    row.sum(100);
    row.tax(600);
    assert.equal(row.tax(), 600);
    row.getterEnabled("tax", true);
    assert.equal(row.tax(), 18);
    row.getterEnabled("tax", false);
    assert.equal(row.tax(), 18);
    row.sum(1000);
    assert.equal(row.tax(), 18);
});

test("prop should return auto value when getter is enabled", function(){
    var DocumentRow = m.define({
        sum : m.field(),
        tax: m.field().getter(function() {             return this.sum()*0.18;         })
    });
    var row = new DocumentRow();
    row.sum(100);
    row.tax(600);
    assert.equal(row.tax(), 600);
    row.getterEnabled("tax", true);
    assert.equal(row.tax(), 18);
    assert.equal(row.getterEnabled("tax"), true);
});

test("hasGetter should return false if getter missing", function(){
    var DocumentRow = m.define({
        sum : m.field(),
        tax: m.field().getter(function(){             return this.sum()*0.18;         })
    });
    var row = new DocumentRow();
    assert.equal(row.hasGetter("sum"), false);
});

test("hasGetter should return true if getter exists ", function(){
    var DocumentRow = m.define({
        sum : m.field(),
        tax: m.field().getter(function(){             return this.sum()*0.18;         })
    });
    var row = new DocumentRow();
    assert.equal(row.hasGetter("tax"), true);
});

test("if getter disabled manually then parent.onChanged is triggered", function(){

    var Child = m.define({
        sum : m.field(),

        tax: m.field().getter(function(){
            return this.sum()*0.18;
        })
    });

    var Document = m.define({          child : m.obj(Child)    });


    var doc = new Document();
    var log = "";
    doc.onChanged(function(){ log +="changed"});
    doc.child().getterEnabled("tax", false);
    assert.equal(log, "changed");
});

test("getter can be disabled/enabled manually", function(){
    var DocumentRow = m.define({
        sum : m.field(),

        tax: m.field().getter(function(){
            return this.sum()*0.18;
        })
    });

    var row = new DocumentRow();
    row.sum(100);
    assert.equal(row.tax(), 18);
    row.getterEnabled("tax", false);
    row.sum(200);
    assert.equal(row.tax(), 18);
    row.getterEnabled("tax", true);
    assert.equal(row.tax(), 36);
});

test("getter turns of after setter", function(){
    var DocumentRow = m.define({
        sum : m.field(),

        tax: m.field().getter(function(){
            return this.sum()*0.18;
        })
    });

    var row = new DocumentRow();
    row.sum(100);
    assert.equal(row.tax(), 18);
    row.sum(200);
    assert.equal(row.tax(), 36);

    row.tax(405);
    row.sum(300);
    assert.equal(row.tax(), 405);
});



test("isNested should return true for nested models", function(){
    var DocumentRow = m.define({
        title : m.field().required().defaultValue("Doc")
    });

    var Document = m.define({
        rows : m.list(DocumentRow),
        title : m.field().required().defaultValue("Doc")

    });
    var d = new Document();
    assert.equal(d.isNested("rows"), true);
    assert.equal(d.isNested("title"), false);
});

test("composite test", function(){


    var Document = m.define({
        rows : m.list(function(){ return DocumentRow;}),
        totalSum :  m.field().getter(function(){
            var sum = 0;
            for (var i = 0; i < this.rows().length; i++) {
                sum += this.rows()[i].sum();
            }
            return sum;
        })
    });

    var DocumentRow = m.define({
        title : m.field().required().defaultValue("Doc"),
        sum : m.field().required().defaultValue(0),
        tax : m.field().getter(function(){
            return this.sum() ? this.sum()*0.18 : null;
        })
    });

    var log = "";
    var d = new Document({marker :'DOC'});
    d.onChanged(function(){
        log+= " changed to " + d.totalSum();
        console.log(" changed to " + d.totalSum());
    });
    d.rows().push({sum :40, marker :'P40'});
    d.rows().push({sum :60, marker :'P60'});
    d.rows().push({sum :80, marker :'P80'});
    assert.equal(log, " changed to 40 changed to 100 changed to 180");
});

test("t251. get error should return error for field", function(){
    var Person = m.define({
        name : m.field().validate(function(){
            if (this.otherName()==this.name())
                return "FAIL";
        }),
        otherName : m.field()
    });
    var p = new Person();
    assert.deepEqual(
        p.getError("name"),
        {msg:"FAIL", code:'default'}
    );
});

test("if validation changed then object changed", function(){
    var Person = m.define({
        name : m.field().validate(function(){
            if (this.otherName()==this.name())
                return "FAIL";
        }),
        otherName : m.field()
    });
    var p = new Person();
    p.name("A");
    p.otherName("B");
    var log = "";
    p.onChanged(function(){
        log+= " changed";
    });
    p.otherName("A");
    assert.equal(log, " changed");
});


test("getVersion returns new version every update", function(){
    var Person = m.define({
        name : m.field()
    });
    var p = new Person();
    var v1 = p.getVersion();
    p.name("A");
    var v2 = p.getVersion();
    assert.notEqual(v1, v2);
    p.name("A");
    var v3 = p.getVersion();
    assert.equal(v3, v2);
});

test('t300 getError should return errors if nested list item failed', function() {
    var Person = m.define({
        addressList : m.list(function(){ return Address;})
    });


    var Address = m.define({
        street : m.field().validate(function(v){
            if (v)
                return null;
            return "Missing";
        }),
        house : m.field().jsonField("HOUSE")
    });

    var p = new Person({addressList:[{}]});
    var errors = p.getErrors(null, true);
    assert.deepEqual(errors.addressList.childErrors,{ '0': { street: { default: { msg: 'Missing', code: 'default' } } } } );
    p.addressList().get(0).street("S");
    assert.deepEqual(p.getErrors(), null);
});

test('getError should return errors if nested object failed', function() {
    var Address = m.define({
        street : m.field().validate(function(v){
            if (v)
                return null;
            return "Missing";
        }),
        house : m.field().jsonField("HOUSE")
    });
    var Person = m.define({
        address : m.obj(Address)
    });

    var p = new Person();
    var errors = p.getErrors();
    assert.deepEqual(errors.address.childErrors, {street:{'default':{msg:"Missing", code:"default"}}});
});


test('getError should return errors if validator failed', function() {
    var Person = m.define({
        name : m.field().validate(function(v){
            if (v)
                return null;
            return "Missing";
        })
    });
    var p = new Person();
    p.name("a");
    assert.deepEqual(p.getErrors(), null);
    p.name(null);
    assert.deepEqual(p.getErrors(), { name : {'default':{msg:"Missing", code:"default"}}    });
});

test('custom errors should vanish when field updated', function() {
    var Person = m.define({
        name : m.field()
    });
    var p = new Person();
    p.setError("name","name is invalid");
    p.name("a");
    assert.deepEqual(p.getErrors(), null);
});

test('custom validation should be supported', function() {
    var Person = m.define({
        name : m.field()
    });
    var p = new Person();
    p.setError("name","name is invalid");
    assert.deepEqual(p.getErrors(), {        name : {'default':{msg:"name is invalid", code:"default"}}    });
});

test('nested prop deps should be supported', function() {
    var Row = m.define({
        sum : m.field().defaultValue(0)
    });
    var Doc = m.define({
        rows : m.list(Row),
        total : m.field().getter(function(x){
            var s = 0;
            for (var i = 0; i < this.rows().length; i++) {
                var itemSum = this.rows()[i].sum();
                s += itemSum ? itemSum : 0;
            }
            return s;
        })
    });
    m.begin();
    var r = new Doc();
    r.rows().push({sum:5});
    r.rows().push({sum:5});
    assert.equal(r.total(),10);
    r.rows().push({sum:5});
    assert.equal(r.total(),15);
    m.end();
});

test('dependent props chaining should be supported', function() {

    var Cube = m.define({
        width : m.field().defaultValue(0),
        height : m.field().defaultValue(0),
        area : m.field().getter(function(){
            return this.width() * this.height();
        }),

        length : m.field().defaultValue(0),
        volume : m.field().getter(function(){
            return this.length() * this.area();
        })
    });
    var r = new Cube();
    assert.equal(r.volume(),0);
    r.setFields({length:8});
    r.setFields({height:2, width : 5});
    assert.equal(r.volume(),80);
});


test('dependent props should be supported for simple fields.', function() {
    var Rect = m.define({
        width : m.field().defaultValue(0),
        height : m.field().defaultValue(0),
        area : m.field().getter(function(){
            return this.width() * this.height();
        })
    });
    var r = new Rect();
    assert.equal(r.area(),0);
    r.update({height:2, width : 5});
    assert.equal(r.area(),10);
});

test('defaults should be supported.', function() {
    var Rect = m.define({
        width : m.field().defaultValue(0),
        height : m.field().defaultValue(0)
    });
    var r = new Rect();
    assert.equal(r.width(),0);
});

test(' JsonField names should be supported in constructor', function() {
    var Address = m.define({         street : m.field(),         house : m.field().jsonField("HOUSE")     });
    var Person = m.define({         name : m.field(),         lastName : m.field(),        addressList : m.list(Address)    });
    var p = new Person({addressList : [{street:"MyStreet","HOUSE" : "hhh"}]});
    assert.equal(p.addressList().get(0).house(),"hhh");
});

test('to json should return json with JsonField names', function() {
    var Address = m.define({         street : m.field(),         house : m.field().jsonField("HOUSE").defaultValue("H1")     });
    var Person = m.define({         name : m.field(),         lastName : m.field(),        addressList : m.list(Address)    });
    var p = new Person({addressList : [{street:"MyStreet"}]});
    assert.deepEqual(        p.toJson(),
        {addressList : [{street:"MyStreet", "HOUSE": "H1"}]});
});

test('to json should return json with default values', function() {
    var Address = m.define({         street : m.field(),         house : m.field().defaultValue("H1")     });
    var Person = m.define({         name : m.field(),         lastName : m.field(),        addressList : m.list(Address)    });
    var p = new Person({addressList : [{street:"MyStreet"}]});
    assert.deepEqual(        p.toJson(),
        {addressList : [{street:"MyStreet", "house": "H1"}]});
});

test('to json should return json', function() {
    var Address = m.define({         street : m.field(),         house : m.field()     });
    var Person = m.define({         name : m.field(),         lastName : m.field(),        addressList : m.list(Address)    });
    var p = new Person({addressList : [{street:"MyStreet", house :"H1"}]});
    assert.deepEqual(p.toJson(), {addressList : [{street:"MyStreet", house :"H1"}]});

});

test('array spilce trigger onchange event', function() {
    var Address = m.define({         street : m.field(),         house : m.field()     });
    var Person = m.define({         name : m.field(),         lastName : m.field(),        addressList : m.list(Address)    });
    var p = new Person({addressList : [{street:"MyStreet", house :"H1"}]});

    var log = "";
    p.onChange("addressList", function(){ log+= " addressList changed"});
    p.addressList().splice(0,1);
    assert.equal(log," addressList changed")
});

test('array setter should update items correctly', function() {
    var Address = m.define({         street : m.field(),         house : m.field()     });
    var Person = m.define({         name : m.field(),         lastName : m.field(),        addressList : m.list(Address)    });
    var p = new Person({addressList : [{street:"MyStreet", house :"H1"}]});
    p.addressList([{street:"MS2",house:"H2"},{street:"MS3",house:"H3"}])
    assert.equal(p.addressList()[0].street(),"MS2");
    assert.equal(p.addressList()[1].street(),"MS3");
});

test('child array should survive setter', function(){
    var Address = m.define({         street : m.field(),         house : m.field()     });
    var Person = m.define({         name : m.field(),         lastName : m.field(),        addressList : m.list(Address)    });
    var p = new Person({addressList : [{street:"MyStreet", house :"H1"}]});
    var a1 = p.addressList();
    p.addressList([
        {street:"NewStreet1"}
    ]);
    var a2 = p.addressList();
    assert.equal(a1,a2);
});

test('child object should survive setter.', function(){
    var Address = m.define({         street : m.field(),         house : m.field()     });
    var Person = m.define({         name : m.field(),         lastName : m.field(),        address : m.obj(Address)    });
    var p = new Person({address : {street:"MyStreet", house :"H1"}});

    assert.equal(p.address().house(),"H1");
    var a1 = p.address();
    p.address({street:"NewStreet1"});
    var a2 = p.address();
    assert.equal(a1,a2);
});

test('can pass json to object setter.', function(){
    var Address = m.define({         street : m.field(),         house : m.field()     });
    var Person = m.define({         name : m.field(),         lastName : m.field(),        address : m.obj(Address)    });
    var p = new Person({address : {street:"MyStreet", house :"H1"}});

    assert.equal(p.address().house(),"H1");
    p.address(        {street:"NewStreet1"}    );
    assert.equal(p.address().house(),null);
    assert.equal(p.address().street(),"NewStreet1");
});

test('can pass deep json to object constructor', function(){
    var Address = m.define({         street : m.field(),         house : m.field()     });
    var Person = m.define({         name : m.field(),         lastName : m.field(),        address : m.obj(Address)    });
    var p = new Person({address : {street:"MyStreet", house :"H1"}});
    assert.equal(p.address().house(),"H1");
});

test('if list passed to setter then onChange triggered.', function(){
    var Address = m.define({         street : m.field(),         house : m.field()     });
    var Person = m.define({         name : m.field(),         lastName : m.field(),        addressList : m.list(Address)    });
    var p = new Person({addressList : [{street:"MyStreet", house :"H1"}]});
    var log = "";
    p.onChange("addressList", function(){ log+= " addressList changed"});

    p.addressList([
        {street:"NewStreet1"},
        {street:"NewStreet2"}
    ]);
    assert.equal(log," addressList changed")
});



test('can pass json to list setter.', function(){
    var Address = m.define({         street : m.field(),         house : m.field()     });
    var Person = m.define({         name : m.field(),         lastName : m.field(),        addressList : m.list(Address)    });
    var p = new Person({addressList : [{street:"MyStreet", house :"H1"}]});
    p.addressList([
        {street:"NewStreet1"},
        {street:"NewStreet2"}
    ]);

    assert.equal(p.addressList()[0].house(),null);
    assert.equal(p.addressList()[1].street(),"NewStreet2");
});

test('can pass json to array constructor', function(){
    var Address = m.define({         street : m.field(),         house : m.field()     });
    var Person = m.define({         name : m.field(),         lastName : m.field(),        addressList : m.list(Address)    });
    var p = new Person({addressList : [{street:"MyStreet"}]});
    assert.equal(p.addressList()[0].street(),"MyStreet");
});

test('can pass json to constructor', function(){
    var Address = m.define({         street : m.field(),         house : m.field()     });
    var a = new Address({street :"MyStreet"});
    assert.equal(a.street(),"MyStreet");
});

test('shift object should update positions.', function() {
    var Address = m.define({         street : m.field(),         house : m.field()     });
    var Person = m.define({         name : m.field(),         lastName : m.field(),        addressList : m.list(Address)    });
    var p = new Person();
    var addressList = p.addressList();
    var a1 = new Address();
    var a2 = new Address();
    var a3 = new Address();
    var a4 = new Address();
    addressList.push(a1);
    addressList.push(a2);
    addressList.push(a3);
    addressList.push(a4);
    var log = "";
    a3.onChanged(function(){ log+= " address #3 changed"});

    m.begin();
    var a1_pop = addressList.shift();
    assert.equal(a1_pop, a1);
    a3.street("S3");
    m.end();
    assert.equal(log, " address #3 changed");
});


test('arrays should trigger onchange.', function() {
    var House = m.define({         num : m.field()     });
    var Address = m.define({         street : m.field(),         house : m.obj(House)     });
    var Person = m.define({         name : m.field(),         lastName : m.field(),        addressList : m.list(Address)    });
    var p = new Person();
    var addressList = p.addressList();
    var a1 = new Address();
    addressList.push(a1);

    var log = "";
    addressList.onChange(function(){ log+= " addressList changed"});
    a1.house().num("S3");
    assert.equal(log, " addressList changed");
});

test('nested items should trigger onchange.', function() {
    var House = m.define({         num : m.field()     });
    var Address = m.define({         street : m.field(),         house : m.obj(House)     });
    var Person = m.define({         name : m.field(),         lastName : m.field(),        addressList : m.list(Address)    });
    var p = new Person();
    var addressList = p.addressList();
    var a1 = new Address();
    addressList.push(a1);

    var log = "";
    p.onChange(function(){ log+= " person changed"});
    a1.house().num("S3");
    assert.equal(log, " person changed");
});




test('push object should import new object.', function() {
    var Address = m.define({         street : m.field(),         house : m.field()     });
    var Person = m.define({         name : m.field(),         lastName : m.field(),        addressList : m.list(Address)    });
    var p = new Person();
    var addressList = p.addressList();
    addressList.push(new Address());
    assert.equal(addressList[0].street(), null);
});

test('push json should import new object.', function() {
    var Address = m.define({         street : m.field(),         house : m.field()     });
    var Person = m.define({         name : m.field(),         lastName : m.field(),        addressList : m.list(Address)    });
    var p = new Person();
    var addressList = p.addressList();
    addressList.push({});
    assert.equal(addressList[0].street(), null);
});
//return;

test('when list defined, default should be empty.', function() {
    var Address = m.define({         street : m.field(),         house : m.field()     });
    var Person = m.define({         name : m.field(),         lastName : m.field(),        addressList : m.list(Address)    });
    var p = new Person();
    var addressList = p.addressList();
    assert.equal(addressList.length, 0);
});

test('when child changed onChanged should be triggered on child.', function() {
    var Address = m.define({         street : m.field(),         house : m.field()     });
    var Person = m.define({         name : m.field(),         lastName : m.field(),        address : m.obj(Address)    });
    var p = new Person();

    var log = "";
    p.address().onChanged(function(){ log+="address changed"});
    p.address().street("AAA");
    assert.equal(log, "address changed");
});


test('when child changed onChanged_PROP should be triggered on child.', function() {
    var Address = m.define({         street : m.field(),         house : m.field()     });
    var Person = m.define({         name : m.field(),         lastName : m.field(),        address : m.obj(Address)    });
    var p = new Person();

    var log = "";
    p.address().onChanged("street", function(){ log+="address changed"});
    p.address().street("AAA");
    assert.equal(log, "address changed");
});

test('when child changed onChanged should be triggered on parent.', function() {
    var Address = m.define({         street : m.field(),         house : m.field()     });
    var Person = m.define({         name : m.field(),         lastName : m.field(),        address : m.obj(Address)    });
    var p = new Person();

    var log = "";
    p.onChanged("address", function(){ log+="address changed"});
    p.address().street("AAA");
    assert.equal(log, "address changed");

});

test('parent should trigger onChange once if child supports transaction.', function() {
    var Address = m.define({         street : m.field(),         house : m.field()     });
    var Person = m.define({         name : m.field(),         lastName : m.field(),        address : m.obj(Address)    });
    var p = new Person();
    var log = "";

    p.onChange("address", function(){ log+="address changed"});
    p.address().begin();
    p.address().street("AAA");
    p.address().house("H1");
    p.address().end();
    assert.equal(log, "address changed");

});

test('parent should trigger onChange when child changed.', function() {
    var Address = m.define({         street : m.field(),         house : m.field()     });
    var Person = m.define({         name : m.field(),         lastName : m.field(),        address : m.obj(Address)    });
    var p = new Person();
    var log = "";

    p.onChange("address", function(){ log+="address changed"});
    p.address().street("AAA");
    assert.equal(log, "address changed");

});

test('nested objects should support get', function() {
    var Address = m.define({
        street : m.field(),
        house : m.field()
    });

    var Person = m.define({
        name : m.field(),
        lastName : m.field(),
        address : m.obj(Address)
    });
    var p = new Person();
    p.address().street("AAA");
    assert.equal(p.address().street(), "AAA");

});

test('nested objects should cache in different values.', function() {
    var Address = m.define({
        street : m.field(),
        house : m.field()
    });

    var Person = m.define({
        name : m.field(),
        lastName : m.field(),
        address : m.obj(Address),
        address2 : m.obj(Address)
    });
    var p = new Person();
    assert.equal(p.address(), p.address());
    assert.equal(p.address2(), p.address2());
    assert.notEqual(p.address2(), p.address());
});

test('nested objects should cache.', function() {
    var Address = m.define({
        street : m.field(),
        house : m.field()
    });

    var Person = m.define({
        name : m.field(),
        lastName : m.field(),
        address : m.obj(Address)
    });
    var p = new Person();
    assert.equal(p.address(), p.address());
});

test('onChanged should reset after transaction.', function() {
    var Person = m.define({
        name : m.field(),
        lastName : m.field()
    });
    var log = "";
    var p = new Person();
    p.onChanged("name", function(v){log+="name-changed-" + v;});
    m.begin();
    p.name("Alex");
    p.name("Peter");
    p.name("Ivan");
    m.end();

    m.begin();
    p.lastName("AAA");
    m.end();

    assert.equal(log,"name-changed-Ivan");
});

test('t808. should trigger onChanged once in __t', function() {
    var Person = m.define({
        name : m.field(),
        lastName : m.field()
    });
    var log = "";
    var p = new Person();
    p.onChanged(function(){log+="name-changed-" + p.name();});
    m.begin();
    p.name("Alex");
    p.name("Peter");
    p.name("Ivan");
    m.end();
    assert.equal(log,"name-changed-Ivan");
});

test('should not trigger onChange if nothing changed', function(){
    var Person = m.define({
        name : m.field(),
        lastName : m.field()
    });
    var log = "";
    var p = new Person();
    p.name("Alex");
    p.onChange("name", function(v){log+="name-changed;";});
    p.name("Alex");
    assert.equal(log,"");

});

test('should support instance onChange events', function(){
    var Person = m.define({
        name : m.field(),
        lastName : m.field()
    });
    var log = "";
    var p = new Person();
    p.onChange("name", function(v){log+="name-changed;";});
    p.onChange(function(v){log+="changed;";});
    p.name("Alex");
    assert.equal(log,"name-changed;changed;");

    log ="";
    p.lastName("Ilyin");
    assert.equal(log,"changed;");

});

test('should support instance onChange events.', function(){
    var Person = m.define({
        name : m.field(),
        lastName : m.field()
    });
    var p = new Person();
    p.onChange("name", function(v){p.lastName(v + "_last")});
    p.name("Alex");
    assert.equal(p.lastName(),"Alex_last");
});

test('should support on change events', function(){
    var Person = m.define({
        name : m.field().onChange(function(v){
            this.lastName(v + "_last");
        }),
        lastName : m.field()
    });
    var p = new Person();
    p.name("Alex");
    assert.equal(p.lastName(),"Alex_last");
});

test('should not crash when model defined', function(){
    m.define({name : m.field()});
});

test('should support getters setter', function(){
    var Person = m.define({name : m.field()});
    var p = new Person();
    p.name("Alex");
    assert.equal(p.name(),"Alex");
});




